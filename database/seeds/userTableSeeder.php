<?php

use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create(array(
            'name' => 'Ashim Batajoo',
            'email' => 'asimbatajoo@gmail.com',
            'password' => \Hash::make('pass'),
        ));

}
}
